export const filmData = [
    {
        "id" : 1,
        "titre": "SPIDER-MAN: NO WAY HOME",
        "acteurs": "Tom Holland, Zendaya, Benedict Cumberbatch",
        "duree": "2h 29min",
        "description": "Pour la première fois dans son histoire cinématographique, Spider-Man, le héros sympa du quartier est démasqué et ne peut désormais plus séparer sa vie normale de ses lourdes responsabilités de super-héros. Quand il demande de l'aide à Doctor Strange, les enjeux deviennent encore plus dangereux, le forçant à découvrir ce qu'être Spider-Man signifie véritablement.",
        "affiche" : "https://fr.web.img2.acsta.net/c_310_420/pictures/21/11/16/10/01/4860598.jpg"
    },
    {
        "id" : 2,
        "titre": "MATRIX RESURRECTIONS",
        "acteurs": "Keanu Reeves, Carrie-Anne Moss, Yahya Abdul-Mateen II",
        "duree": "2h 28min",
        "description" : "MATRIX RESURRECTIONS nous replonge dans deux réalités parallèles – celle de notre quotidien et celle du monde qui s’y dissimule. Pour savoir avec certitude si sa réalité propre est une construction physique ou mentale, et pour véritablement se connaître lui-même, M. Anderson devra de nouveau suivre le lapin blanc.",
        "affiche" : "https://fr.web.img2.acsta.net/c_310_420/pictures/21/11/17/17/24/3336846.jpg"
    },
    {
        "id" : 3,
        "titre": "THE KING'S MAN : PREMIÈRE MISSION",
        "acteurs": "Ralph Fiennes, Gemma Arterton, Rhys Ifans",
        "duree": "2h 11min",
        "description": "Lorsque les pires tyrans et les plus grands génies criminels de l’Histoire se réunissent pour planifier l’élimination de millions d’innocents, un homme se lance dans une course contre la montre pour contrecarrer leurs plans.",
        "affiche" : "https://fr.web.img5.acsta.net/c_310_420/pictures/21/12/14/14/34/1861024.jpg"

    }
]