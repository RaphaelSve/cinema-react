import React, { Fragment } from 'react'
import { BrowserRouter as Router, Routes as Switch, Route, Navigate } from 'react-router-dom'


import Accueil from './pages/Accueil'
import PageErreur from './pages/PageErreur'
import ListeDeFilm from './pages/ListeDeFilms'
import FormulaireAjoutFilm from './pages/FormulaireAjoutFilms'


import BarreNavigation from './BarreNavigation'


let pages = [
    {to: "/accueil", nom:"Accueil"},
    {to: "/films", nom:"Les films"},
    {to: "/nouveauFilm", nom:"Ajouter un film"},

]

export const Routage = (props) => {
    return(
        <Fragment>

        <Router>
            <BarreNavigation pages={pages}></BarreNavigation>
        <Switch>
            <Route path="/" element={<Navigate to="/accueil"/> } />

            <Route path="/accueil" element={<Accueil nomCinema="ABC"/>}></Route>
            <Route path="/films" element={<ListeDeFilm/>}></Route>
            <Route path="/nouveauFilm" element={<FormulaireAjoutFilm/>}></Route>
            <Route path="*" element={<PageErreur />} />
        </Switch>

        </Router>

        </Fragment>
    )
}

export default Routage;