import React, { Fragment, useState } from "react";
import { Link } from "react-router-dom";


export const AjouterUnFilm = (props) => {
  
    const [nouveauFilm, setnouveauFilm] = useState({titreFilm : String, acteurs : String, duree : String, description : String})
  
    let handleTitreFilm = (event) => {
        setnouveauFilm({...nouveauFilm, titreFilm : event.target.value})
    }
    let handleActeurs = (event) => {
        setnouveauFilm({...nouveauFilm, acteurs : event.target.value})
    }
    let handleDuree = (event) => {
        setnouveauFilm({...nouveauFilm, duree : event.target.value})
    }
    let handleDescription = (event) => {
        setnouveauFilm({...nouveauFilm, description : event.target.value})
    }

    let handleSubmit = (event) => {
    event.preventDefault();
    event.stopPropagation();
    props.ajouterFilm(nouveauFilm)
  }

  return (
    <Fragment>
      <form onSubmit={handleSubmit}>
        
      <label>Titre du film : </label>
        <div>
          <input type="text"
                defaultValue={nouveauFilm.titreFilm}
                onChange={handleTitreFilm}      
          />
        </div>
        
      <label>Acteurs : </label>
        <div>
          <input type="text"
                defaultValue={nouveauFilm.acteurs}
                onChange={handleActeurs}
          />
        </div>

      <label>Durée : </label>
        <div>
          <input type="text"
                defaultValue={nouveauFilm.duree}
                onChange={handleDuree}
          />
        </div>
        
      <label>Description : </label>
        <div>

          <textarea cols="30" rows="10"
                defaultValue={nouveauFilm.description}
                onChange={handleDescription}
          >
          </textarea>
        </div>

        <input type="submit" value="Confirmer" />
      </form>
    </Fragment>
  );
};

export default AjouterUnFilm;
