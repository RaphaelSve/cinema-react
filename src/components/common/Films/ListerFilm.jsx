import React, { Fragment, useState } from "react";
import AjouterUnFilm from "../Films/AjouterUnFilm";
import InfoFilm from "../Films/InfoFilm";

export const ListerFilm = () => {
  const [listerFilm, setlisterFilm] = useState([]);

  const handleAjoutFilm = (film) => {
    setlisterFilm([...listerFilm, film]);
  };

  
  const handleEffacer = (e) => {
    e.stopPropagation()
    setlisterFilm([])
  };

  console.log(listerFilm);

  return (
    <Fragment>
      
      <AjouterUnFilm ajouterFilm={handleAjoutFilm}></AjouterUnFilm>

      <div>
        {listerFilm.map((film, index) => (
          <div key={index}>
            <InfoFilm film={film}></InfoFilm>
          </div>
        ))}
          <button onClick={handleEffacer}>Supprimer tout</button>
      </div>

    </Fragment>
  );
};

export default ListerFilm;
