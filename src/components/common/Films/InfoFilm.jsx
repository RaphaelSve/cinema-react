import React, { Fragment } from "react";
import '../../../assets/styles/InfoFilm.css'

export const InfoFilm = (props) => {

    return(
        <Fragment>
            <ul>
               <li>Titre du film : {props.film.titreFilm}</li>
               <li>Acteurs : {props.film.acteurs}</li>
               <li>Durée : {props.film.duree} mins</li>
               <li>Description : {props.film.duree}</li>
            </ul>
        </Fragment>
    )
};

export default InfoFilm;