import React, { Fragment } from 'react'
import { filmData } from '../../../data'
import '../../../assets/styles/FilmAccueil.css'

export const FilmAccueil = () => {

    return(
        <Fragment>

        {filmData.map((data, key) => {
            return (
                <div className='filmConteneur' key={key}>
                    <div className='filmElement'>
                        <p>Titre du film : {data.titre}</p>
                        <p>Description : {data.description}</p>
                        <p>Durée du film : {data.duree}</p>
                        <img src={data.affiche} alt="" width="300x" height="300px" />

                    </div>

                    
                </div>
            )
        })}

        </Fragment>
    )
}

export default FilmAccueil