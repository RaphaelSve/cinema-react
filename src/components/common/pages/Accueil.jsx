import React, { Fragment } from 'react'
import FilmAccueil from '../Films/FilmAccueil';

export const Accueil = (props) => {
        return(
            <Fragment>
                <h3>Bienvenue sur le site du cinéma {props.nomCinema}</h3>

                <div>

                    <p>Films actuellemment à l'affiche</p>

                    <FilmAccueil></FilmAccueil>

                </div>


            </Fragment>
        )
}

export default Accueil;