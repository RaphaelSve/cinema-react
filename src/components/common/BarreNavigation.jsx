import React from 'react'
import {Link} from 'react-router-dom'

import "../../assets/styles/barreNavigation.css"

export const BarreNavigation = (props) => {
    return(
            <nav className='navbar'>
                <div className='navbar-content'>
                {props.pages && props.pages.map( (page, index) => {
                    return (
                        <Link key = { index } to = { page.to }>{ page.nom } </Link>
                    )
                })}
                </div> 
            </nav>
    )
}

export default BarreNavigation;