# Projet cinéma en React

## Ce qui marche : 

- Lister des films sur la page d'accueil à partir d'un tableau de film
- Ajouter un film mais uniquement sur la page courante
- Routage entre les différentes pages
- Supprimer les films

## Ce qui n'est pas fait :

- Envoyer les films ajoutée sur le composant **ListeDeFilms.jsx**
- Page détails avec des informations complémentaires sur un film
